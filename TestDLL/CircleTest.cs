﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDLL
{
    [TestFixture]
    public class CircleTest
    {
        [Test]
        public void TestDiameter()
        {
            double radius = 7;
            Circle c = new Circle(radius);
            Assert.AreEqual(14, c.Diameter);
        }
    }
}