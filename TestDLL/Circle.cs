﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDLL
{
    public class Circle
    {
        public double Radius
        {
            get; private set;
        }

        public double Diameter
        {
            get
            {
                return Radius * 2;
            }
        }

        public double Circumference
        {
            get
            {
                return 2 * Math.PI * Radius;
            }
        }

        public double Area
        {
            get
            {
                return Math.PI * Radius * Radius;
            }
        }

        public Circle(double radius)
        {
            Radius = radius;
        }


    }
}
